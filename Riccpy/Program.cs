﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Riccpy {
    internal class Program {

        static String fullPath = "";

        private static Resources res;


        static void Main(string[] args) {
            Console.WriteLine("Collecting System specs...");

            res = new Resources();
            Program.fullPath = "C:\\users\\" + res.getUser + "\\Desktop\\riccpy.txt";
            Program.WriteFile(Program.fullPath);

            Thread.Sleep(2000);
            
            Console.WriteLine("Done!");
            Console.WriteLine("Exiting in a while..");
            Thread.Sleep(1000);

            Environment.Exit(0);
        }



        private static void WriteFile(String fullPath) {

            // Write file using StreamWriter  
            using (StreamWriter writer = new StreamWriter(fullPath)) {

                writer.WriteLine(res.logTimeStamp + "Collecting system info.....");

                //PCNAME:
                writer.WriteLine(log("PCNAME"));

                //USER:
                writer.WriteLine(log("CURRENTUSER"));

                //GET OS:
                writer.WriteLine(log("OS"));

                //CPU ARCHITECTURE:
                writer.WriteLine(log("CPUARCHITECTURE"));

                //64BIT:
                writer.WriteLine(log("IS64BITOS"));

                //CPU:
                writer.WriteLine(log("CPU"));

                //RAM QUANTITY:
                writer.WriteLine(log("RAMQUANTITY"));

                //RAM TYPE:
                writer.WriteLine(log("RAMTYPE"));

                //GPU
                writer.WriteLine(log("GRAPHICDEVICE"));

                //HDD:
                writer.WriteLine(log("HDD"));


                writer.WriteLine(res.logTimeStamp + "End collecting system info.");
                writer.Write(res.logTimeStamp + "Thankyou for using my software! Rickychii :)");
                
            }

        }

        private static String log(String property) {
            return res.logTimeStamp + property + ": " + res.properties[property];
        }

    }
}