﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace Riccpy {
    
    internal class Resources {

        public Dictionary<String, String> properties;

        public Resources() {
            this.properties = new Dictionary<string, string>() {
                { "PCNAME",this.pcName },
                { "CURRENTUSER", this.getUser },
                { "OS", this.getOS },
                { "CPUARCHITECTURE", this.getCpuArchitecture },
                { "IS64BITOS", this.is64BitOS },
                { "CPU", this.CPUInfo },
                { "RAMQUANTITY", this.ramQuantity },
                { "RAMTYPE", this.ramType },
                { "GRAPHICDEVICE", this.GraphicDevice },
                { "HDD",this.hdd }
            };
        }

        public String getUser {
            get {
                return Environment.UserName;
            }
        }

        public String pcName {
            get {
                return System.Environment.MachineName;
            }
        }

        public String hdd {
            get {

                String hardDrive = String.Empty;

                System.Management.ManagementObjectSearcher ms =
                    new System.Management.ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive");

                foreach (ManagementObject mo in ms.Get()) {
                    Double size = Math.Round((Double)(Double.Parse(mo["Size"].ToString())) / 1024 / 1024 / 1024, 2);
                    hardDrive += (String)mo["Model"] + " " + size.ToString() + "GB  ";
                }

                /*
                foreach (ManagementObject mo in ms.Get())
                {
                    System.Console.Write(mo["Model");
                }

                String hardDrive = "\r\n";

                DriveInfo[] allDrives = DriveInfo.GetDrives();
                foreach (DriveInfo d in allDrives)
                {
                    hardDrive += d.Name + "\r\n";
                    hardDrive += d.DriveType + "\r\n";
                    if (d.IsReady == true)
                    {
                        hardDrive += "Volume label: " + d.VolumeLabel + "\r\n";
                        hardDrive += "File system: " + d.DriveFormat + "\r\n";
                        hardDrive += "Available space to current user: bytes " + d.AvailableFreeSpace + "\r\n";
                        hardDrive += "Total available space: " + d.TotalFreeSpace + "\r\n";
                        hardDrive += "Total size of drive: " + d.TotalSize;

                        hardDrive += "\r\n";
                        hardDrive += "--------";
                        hardDrive += "\r\n";
                    }
                }
                */
                
                return hardDrive; 
            }
        }

        public String ramType {
            get {
                var ram = new ManagementObjectSearcher("select * from Win32_PhysicalMemory")
                .Get()
                .Cast<ManagementObject>()
                .First();

                int ramSpeed = Int32.Parse(ram["Speed"].ToString());
                String ramType = String.Empty;
                if (ramSpeed < 533) ramType = "DDR";
                else if (ramSpeed < 800) ramType = "DDR-2";
                else if (ramSpeed < 2133) ramType = "DDR-3";
                else if (ramSpeed <= 3600) ramType = "DDR-4";
                else ramType = "DDR-5";

                return ramType + " Speed: " + (String)ram["Speed"].ToString() + "MHz";
            }
        }

        public String GraphicDevice {
            get {
                var gpu = new ManagementObjectSearcher("select * from Win32_VideoController")
                .Get()
                .Cast<ManagementObject>()
                .First();

                /*
                    foreach (ManagementObject obj in searcher.Get())
                    {
                        Response.Write("Name  -  " + obj["Name"] + "</br>");
                        Response.Write("DeviceID  -  " + obj["DeviceID"] + "</br>");
                        Response.Write("AdapterRAM  -  " + obj["AdapterRAM"] + "</br>");
                        Response.Write("AdapterDACType  -  " + obj["AdapterDACType"] + "</br>");
                        Response.Write("Monochrome  -  " + obj["Monochrome"] + "</br>");
                        Response.Write("InstalledDisplayDrivers  -  " + obj["InstalledDisplayDrivers"] + "</br>");
                        Response.Write("DriverVersion  -  " + obj["DriverVersion"] + "</br>");
                        Response.Write("VideoProcessor  -  " + obj["VideoProcessor"] + "</br>");
                        Response.Write("VideoArchitecture  -  " + obj["VideoArchitecture"] + "</br>");
                        Response.Write("VideoMemoryType  -  " + obj["VideoMemoryType"] + "</br>");
                    }
                }
                */

                Double ram = 0;
                RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(@"System\\CurrentControlSet\\Control\\Class\\{4d36e968-e325-11ce-bfc1-08002be10318}\\0000");
                ram = Double.Parse(registryKey.GetValue("HardwareInformation.qwMemorySize").ToString());
                Double adapterRam = Math.Round(ram / 1024 / 1024 / 1024, 2);

                return (String)gpu["Name"] + " RAM " + adapterRam.ToString() + "GB" + " Driver Version: " + (String)gpu["DriverVersion"];
            }
        }

        public String CPUInfo {
            get {
                var cpu =
                    new ManagementObjectSearcher("select * from Win32_Processor")
                    .Get()
                    .Cast<ManagementObject>()
                    .First();

                cpu["Name"] = cpu["Name"].ToString()
                   .Replace("(TM)", "™")
                   .Replace("(tm)", "™")
                   .Replace("(R)", "®")
                   .Replace("(r)", "®")
                   .Replace("(C)", "©")
                   .Replace("(c)", "©")
                   .Replace("    ", " ")
                   .Replace("  ", " ");

                /*
                CPU.ID = (string)cpu["ProcessorId"];
                CPU.Socket = (string)cpu["SocketDesignation"];
                CPU.Name = (string)cpu["Name"];
                CPU.Description = (string)cpu["Caption"];
                CPU.AddressWidth = (ushort)cpu["AddressWidth"];
                CPU.DataWidth = (ushort)cpu["DataWidth"];
                CPU.Architecture = (CPU.CpuArchitecture)(ushort)cpu["Architecture"];
                CPU.SpeedMHz = (uint)cpu["MaxClockSpeed"];
                CPU.BusSpeedMHz = (uint)cpu["ExtClock"];
                CPU.L2Cache = (uint)cpu["L2CacheSize"] * (ulong)1024;
                CPU.L3Cache = (uint)cpu["L3CacheSize"] * (ulong)1024;
                CPU.Cores = (uint)cpu["NumberOfCores"];
                CPU.Threads = (uint)cpu["NumberOfLogicalProcessors"];
                */

                return (String)cpu["Name"] + (String)cpu["SocketDesignation"] + " " + cpu["MaxClockSpeed"] + "MHz" + " Cores: " + cpu["NumberOfCores"] + " Threads: " + cpu["NumberOfLogicalProcessors"];
            }
        }

        public String ramQuantity {
            get {
                ulong rambytes = new Microsoft.VisualBasic.Devices.ComputerInfo().TotalPhysicalMemory;
                Double ramGB = (Double)(rambytes / (Double)1024 / (Double)1024 / (Double)1024);
                return (Math.Round(ramGB,2) + " GB");
            }
        }

        public String is64BitOS {
            get {
                return System.Environment.Is64BitOperatingSystem == true ? "Yes" : "No";
            }
        }

        public String getCpuArchitecture {
            get {
                if (IntPtr.Size == 8) return "x86-64";
                else if (IntPtr.Size == 4) return "x86";
                return "";
            }
        }

        public String getOS {
            get {
                string result = string.Empty;
                ManagementObjectSearcher searcher = 
                    new ManagementObjectSearcher("SELECT Caption,BuildNumber FROM Win32_OperatingSystem");
                foreach (ManagementObject os in searcher.Get()) {
                    result = os["Caption"].ToString();
                    break;
                }
                foreach (ManagementObject os in searcher.Get()) {
                    result += " Build " + os["BuildNumber"].ToString();
                }
                return result;
            }
        }

        public String logTimeStamp {
            get {
                return "RICCPY - [" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "]  ";
            }
        }
    }
}